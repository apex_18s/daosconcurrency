package Opgave1;

public class MyThread extends Thread{
	
	private Shared shared;
	public MyThread(String name, Shared obj) {
		this.shared = obj;
	}
	
	public void run() {
		for(int i = 0; i < 100; i++) {
			shared.kritiskSection();
			shared.tagerRandomTid(1000);
		}
		System.out.println(Shared.counter);
	}
}
