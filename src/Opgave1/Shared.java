package Opgave1;

import java.util.Random;

public class Shared {
	public static int counter = 0;
	
	public void tagerRandomTid(int max) {
		Random r = new Random();
		int nyMax = Math.abs(r.nextInt()) % max +1;
		
		for(int i = 0; i < nyMax; i++) {
			for(int j = 0; j < nyMax; j++) {
				int x = 1;
				x = x*x+1;
				x = x-5;
			}
		}
	}
	
	public static int getCounter() {
		return counter;
	}
	
	public void kritiskSection() {
		tagerRandomTid(1000);
		counter++;
	}
}
