package Opgave3;

public class App {

	public static void main(String[] args) {
		Shared shared = new Shared();
		MyThread t1 = new MyThread("t1", shared);
		MyThread t2 = new MyThread("t2", shared);
		
		t1.start();
		t2.start();
	}

}
