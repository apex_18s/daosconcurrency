package Opgave2;

public class App {

	public static void main(String[] args) {
		Shared shared = new Shared();
		MyThread t1 = new MyThread("t1", shared,1);
		MyThread t2 = new MyThread("t2", shared,0);
		
		t1.start();
		t2.start();
	}

}
