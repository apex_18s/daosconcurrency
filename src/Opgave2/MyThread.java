package Opgave2;


public class MyThread extends Thread{
	
	private Shared shared;
	private int thisId;
	private int concurrentId;
	public MyThread(String name, Shared obj, int id) {
		this.shared = obj;
		this.thisId = id;
		this.concurrentId = 0;
	}
	private int other() {
        return thisId == 0 ? 1 : 0;
    }
	
	@Override
	public void run() {
		shared.setFlag(true, thisId);
		shared.setTurn(other());
		while(shared.getFlag(other()) && shared.turn == other()) {
			System.out.println("printing...");
		}
		for(int i = 0; i < 100; i++) {
			shared.kritiskSection();
			shared.tagerRandomTid(1000);
		}
		System.out.println(Shared.counter);
		shared.setFlag(false, thisId);
	}
}
