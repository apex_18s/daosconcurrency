package Opgave2;

import java.util.Random;

public class Shared {
	int turn; 
	boolean flag[];
	public static int counter = 0;
	
	public Shared() {
		flag = new boolean[2];
		flag[0] = false;
		flag[1] = false;
		turn = 0;
	}

	/**
	 * @return the turn
	 */
	public int getTurn() {
		return turn;
	}

	/**
	 * @param turn the turn to set
	 */
	public void setTurn(int turn) {
		this.turn = turn;
	}

	/**
	 * @return the flag
	 */
	public boolean getFlag(int id) {
		return flag[id];
	}

	/**
	 * @param flag the flag to set
	 */
	public void setFlag(boolean flag, int id) {
		this.flag[id] = flag;
	}
	
	public void tagerRandomTid(int max) {
		Random r = new Random();
		int nyMax = Math.abs(r.nextInt()) % max +1;
		
		for(int i = 0; i < nyMax; i++) {
			for(int j = 0; j < nyMax; j++) {
				int x = 1;
				x = x*x+1;
				x = x-5;
			}
		}
	}
	
	public static int getCounter() {
		return counter;
	}
	
	public void kritiskSection() {
		tagerRandomTid(1000);
		counter++;
	}
	
}
